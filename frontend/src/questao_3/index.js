import { useEffect, useState } from "react";

function Questao3() {
  const [valor, setValor] = useState("");

  function handleChange(value) {
    setValor(value);
    if (valor) {
      sessionStorage.setItem("value", value);
    }
  }

  useEffect(() => {
    const valor = sessionStorage.getItem("value") || "";
    setValor(valor);
  }, []);

  return (
    <div>
      <h1>Questão 3</h1>
      <input
        value={valor}
        onChange={(event) => handleChange(event.target.value)}
      />
    </div>
  );
}

export default Questao3;
