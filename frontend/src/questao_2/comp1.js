import { useText } from "./TextContext";

function Comp1() {
  const text = useText();

  return <div>{text}</div>;
}

export default Comp1;
