import Comp1 from "./comp1";
import Comp2 from "./comp2";
import { TextProvider } from "./TextContext";

function Questao2() {
  return (
    <TextProvider>
      <div>
        <h1>Questão 2</h1>
        <Comp1 />
        <Comp2 />
      </div>
    </TextProvider>
  );
}

export default Questao2;
