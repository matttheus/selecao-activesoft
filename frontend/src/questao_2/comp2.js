import { useText, useTextUpdate } from "./TextContext";

function Comp2() {
  const text = useText();
  const setText = useTextUpdate();

  return (
    <div>
      <input value={text} onChange={(event) => setText(event.target.value)} />
    </div>
  );
}

export default Comp2;
