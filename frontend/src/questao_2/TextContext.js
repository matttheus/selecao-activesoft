import { createContext, useContext, useState } from "react";

export const TextContext = createContext();
export const TextUpdateContext = createContext();

export function useText() {
  return useContext(TextContext);
}

export function useTextUpdate() {
  return useContext(TextUpdateContext);
}

export function TextProvider({ children }) {
  const [text, setText] = useState("");

  return (
    <TextContext.Provider value={text}>
      <TextUpdateContext.Provider value={setText}>
        {children}
      </TextUpdateContext.Provider>
    </TextContext.Provider>
  );
}
