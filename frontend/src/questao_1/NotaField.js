import { useState } from "react";
import LightStarIcon from "./star_icon.png";
import DarkStarIcon from "./dark_star_icon.png";

function NotaField({ maxNota, value, onChange }) {
  const [rate, setRate] = useState(
    Array.apply(0, Array(maxNota)).map((_, idx) => {
      return { key: idx + 1, selected: false };
    })
  );

  function handleClick(event) {
    const rated = parseInt(event.target.id);
    const newRate = rate.map((r) => {
      r["selected"] = r.key <= rated ? true : false;
      return r;
    });
    setRate(newRate);
    onChangeValue(event);
  }

  function onChangeValue(event) {
    const nota = parseInt(event.target.id);

    if (nota <= maxNota) {
      onChange(nota);
    }
  }

  return (
    <div>
      {rate.map((r, i) => {
        return (
          <img
            src={r.selected ? LightStarIcon : DarkStarIcon}
            alt="Star Icon"
            onClick={handleClick}
            id={i + 1}
            key={i}
          />
        );
      })}
    </div>
  );
}

export default NotaField;
