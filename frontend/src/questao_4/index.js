async function chamarPromise(x) {
  try {
    if (x > 7) {
      return "Deu certo";
    } else {
      throw "Deu errado";
    }
  } catch (err) {
    return err;
  }
}

function acaoBotao() {
  chamarPromise(8)
    .then((res) => alert(res))
    .catch((err) => alert(err));
}

async function acaoBotao2() {
  try {
    const response = await chamarPromise(8);
    alert(response);
  } catch (error) {
    alert(error);
  }
}

function Questao4() {
  return (
    <div>
      <h1>Questão 4</h1>
      <button onClick={acaoBotao}>Ativar</button>
      <button onClick={acaoBotao2}>Ativar2</button>
    </div>
  );
}

export default Questao4;
