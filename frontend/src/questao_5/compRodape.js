import { useEffect, useRef } from "react";

function CompRodape() {
  const divElement = useRef(null);

  useEffect(() => {
    document.body.appendChild(divElement.current);
  }, []);

  return <div ref={divElement}>Rodapé</div>;
}

export default CompRodape;
