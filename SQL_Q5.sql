SELECT employees.employeeid AS EmployeeId,
       employees.firstname || ' ' || employees.lastname AS EmployeeFullName,
       QtdeCustomers,
       TotalSold,
       MostPopularGenre,
       MostLucrativeGenre
FROM employees
JOIN
    (SELECT employees.EmployeeId AS employee_id_2, COUNT(customers.customerid) AS QtdeCustomers
    FROM employees
    JOIN customers ON employees.employeeid = customers.supportrepid
    GROUP BY employee_id_2
) ON EmployeeId = employee_id_2
JOIN
  (SELECT employees.employeeid AS employee_id_3, Sum(invoices.total) AS TotalSold
   FROM employees
   JOIN customers ON employees.employeeid = customers.supportrepid
   JOIN invoices ON invoices.customerid = customers.customerid
   GROUP BY employee_id_3) ON employee_id_2 = employee_id_3
JOIN
  (SELECT employeeid AS employee_id_4, Max(most_ocurrences), MostPopularGenre
   FROM
     (SELECT employees.employeeid, Sum(invoice_items.quantity) AS most_ocurrences, genres.name AS MostPopularGenre
      FROM employees
      JOIN customers ON employees.employeeid = customers.supportrepid
      JOIN invoices ON invoices.customerid = customers.customerid
      JOIN invoice_items ON invoice_items.invoiceid = invoices.invoiceid
      JOIN tracks ON tracks.trackid = invoice_items.trackid
      JOIN genres ON genres.genreid = tracks.genreid group by employees.employeeid) AS TotalByGender) ON employee_id_3 = employee_id_4
JOIN
  (SELECT employeeid AS employee_id_5, Max(totalsold), MostLucrativeGenre
   FROM
     (SELECT employees.employeeid, Sum(invoices.total) AS totalsold, genres.name AS MostLucrativeGenre
      FROM employees
      JOIN customers ON employees.employeeid = customers.supportrepid
      JOIN invoices ON invoices.customerid = customers.customerid
      JOIN invoice_items ON invoice_items.invoiceid = invoices.invoiceid
      JOIN tracks ON tracks.trackid = invoice_items.trackid
      JOIN genres ON genres.genreid = tracks.genreid GROUP BY employees.employeeid) AS TotalByGender) ON employee_id_4 = employee_id_5
