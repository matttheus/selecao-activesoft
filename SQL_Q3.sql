SELECT g.name AS Genre, SUM(ii.Quantity*ii.UnitPrice) AS TotalSold, COUNT(*) AS QtdeSold
FROM invoice_items AS ii
JOIN tracks AS t ON t.TrackId = ii.TrackId
JOIN genres AS g ON g.GenreId = t.GenreId
GROUP BY Genre