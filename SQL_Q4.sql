SELECT a.Title as AlbumTitle, SUM(t.UnitPrice) AS Price, SUM(t.Milliseconds / 60000) AS Duration_minutes, SUM(t.Bytes / 1024 / 1024) AS Size_MB 
FROM albums AS a
JOIN tracks t ON t.AlbumId = a.AlbumId
GROUP BY AlbumTitle