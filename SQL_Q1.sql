SELECT artists.Name as ArtistName, count(*) as QtdeAlbums
FROM artists
JOIN albums ON albums.ArtistId = artists.ArtistId
GROUP BY ArtistName
ORDER BY QtdeAlbums;