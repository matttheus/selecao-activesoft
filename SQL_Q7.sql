select employees.employeeid, employees.firstname || ' ' || employees.lastname as EmployeeFullName,
count(customers.customerid) as QtdeCustomers,
SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '01' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Jan, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '02' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Feb, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '03' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Mar, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '04' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Apr, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '05' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_May, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '06' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Jun, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '07' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Jul, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '08' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Aug, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '09' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Sep, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '10' 
      THEN
         invoices.total 
      ELSE
         0 
   END
) TotalSold_Oct, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '11' 
      THEN
         invoices.total
      ELSE
         0 
   END
) TotalSold_Nov, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '12' 
      THEN
         invoices.total
      ELSE
         0 
   END
) TotalSold_DEC,
SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '01' 
    		AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0
   END
) QtdeTracksSold_Jan, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '02'
    		AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
        1
      ELSE
         0 
   END
) QtdeTracksSold_Feb, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '03' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Mar, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '04' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Apr, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '05' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_May, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '06' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1 
      ELSE
         0 
   END
) QtdeTracksSold_Jun, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '07' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1 
      ELSE
         0 
   END
) QtdeTracksSold_Jul, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '08' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Aug, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '09' AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Sep, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '10'
   			AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Oct, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '11'
  			AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_Nov, COUNT( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '12' 
  			AND invoice_items.InvoiceId = invoices.invoiceid
      THEN
         1
      ELSE
         0 
   END
) QtdeTracksSold_DEC
from employees
join customers on customers.supportrepid = employees.employeeid
join invoices on customers.customerid = invoices.customerid
join invoice_items on invoice_items.invoiceid = invoices.invoiceid
group by employees.employeeid
