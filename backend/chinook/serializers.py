from rest_framework import serializers
from chinook.models import Album, Artist,  Customers, Genre, Playlist, Track
from core.utils import sql_fetch_one
from django.db import connection


class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = '__all__'
        depth = 1


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):
    tracks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Genre
        fields = '__all__'


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = '__all__'


class TrackSimplifiedSerializer(serializers.ModelSerializer):
    album_name = serializers.CharField(source='album.title', read_only=True)
    duration = serializers.SerializerMethodField()

    def get_duration(self, obj):
        total_seconds = obj.milliseconds/1000

        minutes = total_seconds // 60
        seconds = total_seconds - minutes*60

        return f'{minutes}min {seconds}s'

    class Meta:
        model = Track
        fields = ('id', 'name', 'composer', 'album_name', 'duration')


class PlaylistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Playlist
        fields = ('name',)


class CustomerSimplifiedSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = Customers
        fields = ['first_name', 'last_name', 'email']


class TotalSpendSerializer(serializers.ModelSerializer):
    year = serializers.DateField(input_formats=['%Y', 'iso-8601'])
    customer_id = serializers.PrimaryKeyRelatedField(
        queryset=Customers.objects.all())

    class Meta:
        model = Customers
        fields = ['customer_id', 'year']
    
    def validate_year(self, _date):
        if _date.year < 1970 or _date.year > 2100:
            raise serializers.ValidationError("Invalid year, try a year between 1970 and 2100")
        return _date

    def to_representation(self, instance):
        customer_id = instance.get('customer_id').id
        year = str(instance.get('year').year)
        
        return sql_fetch_one("""
            SELECT ROUND(SUM(i.total), 4) AS total FROM invoices i
            JOIN customers c ON c.CustomerId = i.CustomerId
            WHERE strftime('%%Y', i.InvoiceDate) = %s
              AND c.CustomerId = %s
        """, [year, customer_id])
