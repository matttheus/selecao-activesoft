from rest_framework.generics import ListAPIView, GenericAPIView, CreateAPIView
from rest_framework.response import Response

from chinook.serializers import (
    AlbumSerializer,
    GenreSerializer, 
    PlaylistSerializer, 
    TrackSimplifiedSerializer,
    CustomerSimplifiedSerializer,
    TotalSpendSerializer
)
from chinook.models import Album, Customers, Genre, Playlist, Track
from core.utils import sql_fetch_all


class AlbumListAPIView(ListAPIView):
    queryset = Album.objects.all().select_related("artist")
    serializer_class = AlbumSerializer


class GenreListAPIView(ListAPIView):
    queryset = Genre.objects.all().prefetch_related("tracks")
    serializer_class = GenreSerializer


class PlaylistListAPIView(ListAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer


class TrackListAPIView(ListAPIView):
    queryset = Track.objects.select_related("album")
    serializer_class = TrackSimplifiedSerializer


class ReportDataAPIView(GenericAPIView):
    def get(self, request, *args, **kwargs):
        
        dado = sql_fetch_all(
            """
                SELECT *,
                    customers.[FirstName] || ' ' || customers.[LastName] as FullName
                FROM customers
                LEFT JOIN employees ON employees.[EmployeeId] = customers.[SupportRepId]
                ORDER BY CustomerId
            """
        )

        return Response(dado)


class CustomerSimplifiedAPIView(CreateAPIView):
    queryset = Customers.objects.all()
    serializer_class = CustomerSimplifiedSerializer


class TotalSpendAPIView(GenericAPIView):
    queryset = Customers.objects.all()
    serializer_class = TotalSpendSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)