SELECT
   genres.GenreId,
   genres.Name,
   SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '01' 
      THEN
         1 
      ELSE
         0 
   END
) Jan, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '02' 
      THEN
         1 
      ELSE
         0 
   END
) Feb, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '03' 
      THEN
         1 
      ELSE
         0 
   END
) Mar, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '04' 
      THEN
         1 
      ELSE
         0 
   END
) Apr, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '05' 
      THEN
         1 
      ELSE
         0 
   END
) May, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '06' 
      THEN
         1 
      ELSE
         0 
   END
) Jun, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '07' 
      THEN
         1 
      ELSE
         0 
   END
) Jul, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '08' 
      THEN
         1 
      ELSE
         0 
   END
) Aug, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '09' 
      THEN
         1 
      ELSE
         0 
   END
) Sep, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '10' 
      THEN
         1 
      ELSE
         0 
   END
) Oct, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '11' 
      THEN
         1 
      ELSE
         0 
   END
) Nov, SUM( 
   CASE
      WHEN
         strftime('%m', invoices.InvoiceDate) = '12' 
      THEN
         1 
      ELSE
         0 
   END
) DEC 
FROM
   genres 
   JOIN
      tracks 
      ON tracks.GenreId = genres.GenreId 
   JOIN
      invoice_items 
      ON invoice_items.TrackId = tracks.TrackId 
   JOIN
      invoices 
      ON invoices.InvoiceId = invoice_items.InvoiceId 
WHERE
   strftime('%Y', invoices.InvoiceDate) = '2010' 
GROUP BY
   genres.GenreId