SELECT (FirstName || ' ' || LastName) AS CustomerFullName, SUM(invoices.Total) AS Total 
FROM customers
JOIN invoices ON invoices.CustomerId = customers.CustomerId
GROUP BY CustomerFullName
ORDER BY Total DESC
LIMIT 20